# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path, re_path
from app import views

urlpatterns = [
    # Matches any html file 
    re_path(r'^.*\.html', views.pages, name='pages'),

    # The home page
    path('', views.index, name='home'),
    path('upload-file-page', views.uploadFilePage, name='upload_file_page'),
    path('read-file', views.readFile, name='read_file'),
    path('venus-dashboard', views.renderVenusDashboard, name='venus_dashboard'),
    path('read-file-venus', views.readFileVenus, name='read_file_venus'),
    path('read-file-grni', views.readFileVenusGRNI, name='read-file-grni'),
    path('read-file-opo', views.readFileVenusOPO, name='read-file-opo'),
    path('read-file-poa', views.readFileVenusPOA, name='read-file-poa'),
    path('edit-grni/<int:id>', views.editGRNI, name='edit-grni'),
    path('edit-opo/<int:id>', views.editOPO, name='edit-opo'),
    path('edit-poa/<int:id>', views.editPOA, name='edit-poa'),
    path('create-grni', views.createGRNI, name='create-grni'),
    path('create-opo', views.createOPO, name='create-opo'),
    path('create-poa', views.createPOA, name='create-poa'),
    path('delete-grni/<int:id>', views.deleteGRNI, name='delete-grni'),
    path('delete-opo/<int:id>', views.deleteOPO, name='delete-opo'),
    path('delete-poa/<int:id>', views.deletePOA, name='delete-poa'),
    path('restore-grni/<int:id>', views.restoreGRNI, name='restore-grni'),
    path('restore-opo/<int:id>', views.restoreOPO, name='restore-opo'),
    path('restore-poa/<int:id>', views.restorePOA, name='restore-poa'),

    # API for charts
    path('api/total_completeness', views.api_total_completeness, name='total_completeness'),
    path('api/total_completeness_by_fy', views.api_total_completeness_by_fy, name='total_completeness_by_fy'),
    path('api/contracts_by_category', views.api_contracts_by_category, name='contracts_by_category'),
    path('api/target_actual', views.api_target_actual, name='target_actual'),
    
    path('api/venus_open_po', views.api_venus_open_po, name='venus_open_po'),
    path('api/venus_grni', views.api_venus_grni, name='venus_grni'),
    path('api/venus_po_after', views.api_venus_po_after, name='venus_po_after'),
    path('venus-table', views.venusTable, name='venus-table'),
    path('api/venus_grni_table', views.api_venus_grni_table.as_view(), name='venus-grni-table'),
    path('api/venus_opo_table', views.api_venus_opo_table.as_view(), name='venus-opo-table'),
    path('api/venus_poa_table', views.api_venus_poa_table.as_view(), name='venus-poa-table'),
    path('api/venus_grni_export_csv', views.venus_grni_export_csv, name='venus-grni-export-csv'),
    path('api/venus_grni_export_xlsx', views.venus_grni_export_xlsx, name='venus-grni-export-xlsx'),
]
