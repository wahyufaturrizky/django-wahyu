from app.models import *
from django.contrib.humanize.templatetags.humanize import intcomma

import datetime 
import openpyxl

MONTH_FY = ["08", "09", "10", "11", "12", "01", "02", "03", "04", "05", "06", "07"]

SHORT_MONTH = ['', 'Jan.', 'Feb.', 'Mar.', 'Apr.', 'Mei.',
         'Jun.', 'Jul.', 'Agu.', 'Sep.', 'Okt.', 'Nov.', 'Des.']

LONG_MONTH = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
              'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
              
def column(matrix, i):
    return [row[i] for row in matrix]

def renderRupiah(rupiahs):
    is_negative = True if rupiahs<0 else False
    rupiahs = abs(round(float(rupiahs), 2))
    temp = "Rp%s%s" % (intcomma(int(rupiahs)), ("%0.2f" % rupiahs)[-3:])
    temp = temp.replace('.', 'x')
    temp = temp.replace(',', '.')
    temp = temp.replace('x', ',')
    if is_negative:
        temp = f'({temp})'
    return temp

def getMonthShort(date):
    return f'{SHORT_MONTH[date.month]}'
