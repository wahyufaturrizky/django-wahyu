def trackingActualtoList(worksheet):
    excel_data = list()
    # iterating over the rows and
    # getting value from each cell in row
    for row in worksheet.iter_rows(5, 121, 3, 63):
        row_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_data.append(None)
            else:
                row_data.append(cell.value)
        excel_data.append(row_data)
    return excel_data

def numberOfCalltoListTargetData(worksheet):
    target_data = list()
    # iterating over the rows and
    # getting value from each cell in row
    for row in worksheet.iter_rows(4, 10, 2, 16):
        row_target_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_target_data.append(None)
            else:
                row_target_data.append(cell.value)
        target_data.append(row_target_data)

    for row in worksheet.iter_rows(12, 18, 2, 16):
        row_target_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_target_data.append(None)
            else:
                row_target_data.append(cell.value)
        target_data.append(row_target_data)

    for row in worksheet.iter_rows(31, 37, 2, 16):
        row_target_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_target_data.append(None)
            else:
                row_target_data.append(cell.value)
        target_data.append(row_target_data)

    for row in worksheet.iter_rows(39, 45, 2, 16):
        row_target_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_target_data.append(None)
            else:
                row_target_data.append(cell.value)
        target_data.append(row_target_data)

    for row in worksheet.iter_rows(58, 64, 2, 16):
        row_target_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_target_data.append(None)
            else:
                row_target_data.append(cell.value)
        target_data.append(row_target_data)

    for row in worksheet.iter_rows(66, 72, 2, 16):
        row_target_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_target_data.append(None)
            else:
                row_target_data.append(cell.value)
        target_data.append(row_target_data)
    
    return target_data

def numberOfCalltoListActualData(worksheet):
    actual_data = list()
    # iterating over the rows and
    # getting value from each cell in row
    for row in worksheet.iter_rows(4, 10, 19, 33):
        row_actual_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_actual_data.append(None)
            else:
                row_actual_data.append(cell.value)
        actual_data.append(row_actual_data)

    for row in worksheet.iter_rows(12, 18, 19, 33):
        row_actual_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_actual_data.append(None)
            else:
                row_actual_data.append(cell.value)
        actual_data.append(row_actual_data)

    for row in worksheet.iter_rows(31, 37, 19, 33):
        row_actual_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_actual_data.append(None)
            else:
                row_actual_data.append(cell.value)
        actual_data.append(row_actual_data)

    for row in worksheet.iter_rows(39, 45, 19, 33):
        row_actual_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_actual_data.append(None)
            else:
                row_actual_data.append(cell.value)
        actual_data.append(row_actual_data)

    for row in worksheet.iter_rows(58, 64, 19, 33):
        row_actual_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_actual_data.append(None)
            else:
                row_actual_data.append(cell.value)
        actual_data.append(row_actual_data)

    for row in worksheet.iter_rows(66, 72, 19, 33):
        row_actual_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_actual_data.append(None)
            else:
                row_actual_data.append(cell.value)
        actual_data.append(row_actual_data)
    
    return actual_data

def openPotoList(worksheet):
    excel_data = list()
    # iterating over the rows and
    # getting value from each cell in row
    for row in worksheet.iter_rows(2, 1149, 2, 26):
        row_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_data.append(None)
            else:
                row_data.append(cell.value)
        excel_data.append(row_data)
    return excel_data

def grnitoList(worksheet):
    excel_data = list()
    # iterating over the rows and
    # getting value from each cell in row
    for row in worksheet.iter_rows(3, 456, 3, 23):
        row_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_data.append(None)
            else:
                row_data.append(cell.value)
        excel_data.append(row_data)
    return excel_data

def poAftertoList(worksheet):
    excel_data = list()
    # iterating over the rows and
    # getting value from each cell in row
    for row in worksheet.iter_rows(2, 219, 2, 21):
        row_data = list()
        for cell in row:
            if (cell.value == '#N/A' or cell.value == '"NA"'):
                row_data.append(None)
            else:
                row_data.append(cell.value)
        excel_data.append(row_data)
    return excel_data