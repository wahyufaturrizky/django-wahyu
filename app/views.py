# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from app.database_render import *
from app.process_excel import *
from app.db import *
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect

from app.utils import MONTH_FY, renderRupiah
from app.models import Contract_tracking, Contract_period_coverage, Contract_commodity_group, Contract_buyer, Contract_actual, Contract_target
# Venus models
from app.models import Venus_po_type, Venus_department, Venus_business_unit, Venus_open_po, Venus_grni, Venus_po_after, Venus_grni_raw, Venus_opo_raw
from django.db.models import Avg, Count, Min, Sum
import json
import csv
from datetime import datetime, date
from io import StringIO
from app.grni_logic import procData
from app.forms import RawGRNIForm, RawOPOForm, RawPOAForm

from django_datatables_view.base_datatable_view import BaseDatatableView
from django.utils.html import escape
from urllib.parse import parse_qs

import openpyxl


@login_required(login_url="/login/")
def index(request):
    buyers = [(buyer.name, buyer.id) for buyer in Contract_buyer.objects.all()]
    commodity_group = [
        (commodity.name, commodity.id) for commodity in Contract_commodity_group.objects.all()]
    context = {
        "buyers": buyers,
        "commodity_group": commodity_group
    }
    return render(request, "index.html", context=context)


@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:

        load_template = request.path.split('/')[-1]
        template = loader.get_template('pages/' + load_template)
        return HttpResponse(template.render(context, request))

    except:

        template = loader.get_template('pages/error-404.html')
        return HttpResponse(template.render(context, request))


@login_required(login_url="/login/")
def uploadFilePage(request):
    return render(request, "pages/ui-upload_file.html")


@login_required(login_url="/login/")
def renderVenusDashboard(request):
    po_type = [(item.id, item.name) for item in Venus_po_type.objects.all()]
    user = [(item.id, item.name) for item in Venus_department.objects.all()]
    bu = [(item.code, item.name) for item in Venus_business_unit.objects.all()]
    context = {
        "po_type": po_type,
        "user": user,
        "bu": bu
    }
    return render(request, "pages/index-venus.html")


@login_required(login_url="/login/")
def readFile(request):
    excel_file = request.FILES["excel_file"]

    wb = openpyxl.load_workbook(excel_file, data_only=True)

    # getting a particular sheet by name out of many sheets
    worksheet = wb["Tracking Actual"]
    print(worksheet)

    tracking_data = trackingActualtoList(worksheet)

    worksheet = wb["# Call"]

    # process actual and target data
    target_data = numberOfCalltoListTargetData(worksheet)
    actual_data = numberOfCalltoListActualData(worksheet)

    # delete data on DB when uploading
    truncate_contract_db()

    # insert data to models
    insertContractDatatoDB(tracking_data)
    insertTargetDatatoDB(target_data)
    insertActualDatatoDB(actual_data)

    return render(request, 'pages/ui-upload_file.html', {"excel_data": actual_data})


@login_required(login_url="/login/")
def readFileVenus(request):
    excel_file = request.FILES["excel_file"]

    wb = openpyxl.load_workbook(excel_file, data_only=True)

    # getting a particular sheet by name out of many sheets
    worksheet = wb["OpenPO"]
    print(worksheet)

    # process open_po data
    open_po = openPotoList(worksheet)

    worksheet = wb["GRNI"]
    print(worksheet)

    grni = grnitoList(worksheet)

    worksheet = wb["PO AFTER"]
    print(worksheet)

    po_after = poAftertoList(worksheet)

    # delete data on DB when uploading
    truncate_venus_db()

    # insert data to models
    insertVenusDatatoDB(open_po, grni, po_after)

    return render(request, 'pages/ui-upload_file.html', {"excel_data": po_after})


@login_required(login_url="/login/")
def readFileVenusGRNI(request):
    start_time = datetime.now()
    HEADER_ROW = 6
    csv_file = request.FILES["csv_file"].read().decode('utf-8')
    csv_reader = csv.reader(StringIO(csv_file), delimiter=',')
    row_num = 1

    for row in csv_reader:
        if row_num > HEADER_ROW:
            # get data
            po_type = row[0] if row[0] != '' else None
            po_number = row[1] if row[1] != '' else None
            po_date = row[2] if row[2] != '' else None
            doc_type = row[3] if row[3] != '' else None
            doc_number = row[4] if row[4] != '' else None
            doc_date = row[5] if row[5] != '' else None
            supplier_code = row[6] if row[6] != '' else None
            supplier_name = row[7] if row[7] != '' else None
            remark = row[8] if row[8] != '' else None
            po_currency = row[9] if row[9] != '' else None
            amount = row[10] if row[10] != '' else None
            unit = row[10] if row[11] != '' else None

            # cast data
            po_number = int(po_number) if po_number != None else po_number
            doc_number = int(doc_number) if doc_number != None else doc_number
            supplier_code = int(
                supplier_code) if supplier_code != None else supplier_code
            amount = float(amount) if amount != None else amount
            unit = float(unit) if unit != None else unit
            po_date = datetime.strptime(
                po_date, '%d/%m/%Y').date() if po_date != None else po_date
            doc_date = datetime.strptime(
                doc_date, '%d/%m/%Y').date() if doc_date != None else doc_date

            data = {
                "po_type": po_type,
                "po_number": po_number,
                "po_date": po_date,
                "doc_type": doc_type,
                "doc_number": doc_number,
                "doc_date": doc_date,
                "supplier_code": supplier_code,
                "supplier_name": supplier_name,
                "remark": remark,
                "po_currency": po_currency,
                "amount": amount,
                "unit": unit,
            }
            item = Venus_grni_raw.objects.create(**data)
            item.save()
        row_num += 1
    print((datetime.now() - start_time).seconds)

    procData(Venus_grni_raw.objects.all())

    return render(request, 'pages/ui-upload_file.html')


@login_required(login_url="/login/")
def readFileVenusOPO(request):
    start_time = datetime.now()
    HEADER_ROW = 3
    csv_file = request.FILES["csv_file"].read().decode('utf-8')
    csv_reader = csv.reader(StringIO(csv_file), delimiter=',')
    row_num = 1

    for row in csv_reader:
        if row_num > HEADER_ROW:
            # get data
            order_number = row[0] if row[0] != '' else None
            or_ty = row[1] if row[1] != '' else None
            amount_to_receive = row[2] if row[2] != '' else None
            ordered_amount = row[3] if row[3] != '' else None
            order_co = row[4] if row[4] != '' else None
            supplier_number = row[5] if row[5] != '' else None
            supplier_name = row[6] if row[6] != '' else None
            line_description = row[7] if row[7] != '' else None
            last_status = row[8] if row[8] != '' else None
            next_status = row[9] if row[9] != '' else None
            gl_date = row[10] if row[10] != '' else None
            order_date = row[11] if row[11] != '' else None
            quantity_to_receive = row[12] if row[12] != '' else None
            unit_cost = row[13] if row[13] != '' else None
            original_order_no = row[14] if row[14] != '' else None
            orig_ord_type = row[15] if row[15] != '' else None
            account_number = row[16] if row[16] != '' else None
            cost_center = row[17] if row[17] != '' else None
            obj_acct = row[18] if row[18] != '' else None
            line_number = row[19] if row[19] != '' else None
            managrl_code_1 = row[20] if row[20] != '' else None
            managrl_code_2 = row[21] if row[21] != '' else None
            managrl_code_3 = row[22] if row[22] != '' else None
            managrl_code_4 = row[23] if row[23] != '' else None
            second_item_number = row[24] if row[24] != '' else None

            # cast data
            order_number = int(
                order_number) if order_number != None else order_number
            order_co = int(order_co) if order_co != None else order_co
            supplier_number = int(
                supplier_number) if supplier_number != None else supplier_number
            last_status = int(
                last_status) if last_status != None else last_status
            next_status = int(
                next_status) if next_status != None else next_status
            original_order_no = int(
                original_order_no) if original_order_no != None else original_order_no
            obj_acct = int(obj_acct) if obj_acct != None else obj_acct
            line_number = int(
                line_number) if line_number != None else line_number
            amount_to_receive = float(
                amount_to_receive) if amount_to_receive != None else amount_to_receive
            ordered_amount = float(
                ordered_amount) if ordered_amount != None else ordered_amount
            quantity_to_receive = float(
                quantity_to_receive) if quantity_to_receive != None else quantity_to_receive
            unit_cost = float(unit_cost) if unit_cost != None else unit_cost
            cost_center = float(
                cost_center) if cost_center != None else cost_center
            gl_date = datetime.strptime(
                gl_date, '%d/%m/%Y').date() if gl_date != None else gl_date
            order_date = datetime.strptime(
                order_date, '%d/%m/%Y').date() if order_date != None else order_date

            data = {
                "order_number": order_number,
                "or_ty": or_ty,
                "amount_to_receive": amount_to_receive,
                "ordered_amount": ordered_amount,
                "order_co": order_co,
                "supplier_number": supplier_number,
                "supplier_name": supplier_name,
                "line_description": line_description,
                "last_status": last_status,
                "next_status": next_status,
                "gl_date": gl_date,
                "order_date": order_date,
                "quantity_to_receive": quantity_to_receive,
                "unit_cost": unit_cost,
                "original_order_no": original_order_no,
                "orig_ord_type": orig_ord_type,
                "account_number": account_number,
                "cost_center": cost_center,
                "obj_acct": obj_acct,
                "line_number": line_number,
                "managrl_code_1": managrl_code_1,
                "managrl_code_2": managrl_code_2,
                "managrl_code_3": managrl_code_3,
                "managrl_code_4": managrl_code_4,
                "second_item_number": second_item_number
            }

            item = Venus_opo_raw.objects.create(**data)
            item.save()
        row_num += 1
    print((datetime.now() - start_time).seconds)

    # procData(Venus_grni_raw.objects.all())

    return render(request, 'pages/ui-upload_file.html')


@login_required(login_url="/login/")
def readFileVenusPOA(request):
    start_time = datetime.now()
    HEADER_ROW = 4
    csv_file = request.FILES["csv_file"].read().decode('utf-8')
    csv_reader = csv.reader(StringIO(csv_file), delimiter=',')
    row_num = 1

    for row in csv_reader:
        if row_num > HEADER_ROW:
            # get data
            company = row[0] if row[0] != '' else None
            payment_ref = row[1] if row[1] != '' else None
            voucher_gl_date = row[2] if row[2] != '' else None
            supplier_invoice_number = row[3] if row[3] != '' else None
            invoice_date = row[4] if row[4] != '' else None
            payment_gl_date = row[5] if row[5] != '' else None
            voucher_doc_type = row[6] if row[6] != '' else None
            voucher_number = row[7] if row[7] != '' else None
            po_number = row[8] if row[8] != '' else None
            po_type = row[9] if row[9] != '' else None
            po_date = row[10] if row[10] != '' else None
            po_description_line_1 = row[11] if row[11] != '' else None
            po_description_line_2 = row[12] if row[12] != '' else None
            vendor_code = row[13] if row[13] != '' else None
            vendor_name = row[14] if row[14] != '' else None
            item_code = row[15] if row[15] != '' else None
            item_description = row[16] if row[16] != '' else None
            stocking_type = row[17] if row[17] != '' else None
            stocking_type_description = row[18] if row[18] != '' else None
            account_code = row[19] if row[19] != '' else None
            account_description = row[20] if row[20] != '' else None
            business_unit = row[21] if row[21] != '' else None
            amount = row[22] if row[22] != '' else None
            domestic_currency_code = row[23] if row[23] != '' else None
            currency_code = row[24] if row[24] != '' else None
            search_type = row[25] if row[25] != '' else None

            # cast data
            company = int(company) if company != None else company
            payment_ref = int(
                payment_ref) if payment_ref != None else payment_ref
            voucher_number = int(
                voucher_number) if voucher_number != None else voucher_number
            po_number = int(po_number) if po_number != None else po_number
            vendor_code = int(
                vendor_code) if vendor_code != None else vendor_code
            business_unit = int(
                business_unit) if business_unit != None else business_unit
            amount = float(amount) if amount != None else amount
            voucher_gl_date = datetime.strptime(
                voucher_gl_date, '%d/%m/%Y').date() if voucher_gl_date != None else voucher_gl_date
            po_date = datetime.strptime(
                po_date, '%d/%m/%Y').date() if po_date != None else po_date
            invoice_date = datetime.strptime(
                invoice_date, '%d/%m/%Y').date() if invoice_date != None else invoice_date

            data = {
                "company": company,
                "payment_ref": payment_ref,
                "voucher_gl_date": voucher_gl_date,
                "supplier_invoice_number": supplier_invoice_number,
                "invoice_date": invoice_date,
                "payment_gl_date": payment_gl_date,
                "voucher_doc_type": voucher_doc_type,
                "voucher_number": voucher_number,
                "po_number": po_number,
                "po_type": po_type,
                "po_date": po_date,
                "po_description_line_1": po_description_line_1,
                "po_description_line_2": po_description_line_2,
                "vendor_code": vendor_code,
                "vendor_name": vendor_name,
                "item_code": item_code,
                "item_description": item_description,
                "stocking_type": stocking_type,
                "stocking_type_description": stocking_type_description,
                "account_code": account_code,
                "account_description": account_description,
                "business_unit": business_unit,
                "amount": amount,
                "domestic_currency_code": domestic_currency_code,
                "currency_code": currency_code,
                "search_type": search_type
            }

            item = Venus_poa_raw.objects.create(**data)
            item.save()
        row_num += 1
    print((datetime.now() - start_time).seconds)

    # procData(Venus_grni_raw.objects.all())

    return render(request, 'pages/ui-upload_file.html')


@login_required(login_url="/login/")
def venusTable(request):
    return render(request, "pages/view-table-venus.html")


def api_total_completeness(request):
    if request.method in ["POST", "GET"]:
        if request.body != b'':
            post_data = json.loads(request.body)
        # insert filter buyer, commodity and direct here
        qs = Contract_tracking.objects.all()
        if request.method == "POST":
            qs = filterQs(qs, post_data)
        completed = qs.filter(
            completed_count=True).count()
        uncompleted = qs.filter(
            completed_count=False).count()
        data = {
            'completed': completed,
            'uncompleted': uncompleted
        }
        return JsonResponse(data)


def api_total_completeness_by_fy(request):
    if request.method in ["POST", "GET"]:
        if request.body != b'':
            post_data = json.loads(request.body)
        data = {
            "labels": [],
            "uncompleted": [],
            "completed": [],
        }
        all_fy = Contract_period_coverage.objects.all().order_by('period')
        for fy in all_fy:
            period = fy.period
            qs = fy.contract_tracking_set.all()
            if request.method == "POST":
                qs = filterQs(qs, post_data)
            completed_in_fy = qs.filter(
                completed_count=True).count()
            uncompleted_in_fy = qs.filter(
                completed_count=False).count()
            data["labels"].append(period)
            data["uncompleted"].append(uncompleted_in_fy)
            data["completed"].append(completed_in_fy)
        return JsonResponse(data)


def api_contracts_by_category(request):
    if request.method in ["POST", "GET"]:
        if request.body != b'':
            post_data = json.loads(request.body)
        data = {
            "comodity_name": [],
            "number_of_contracts": [],
            "uncompleted": [],
            "completed": [],
            "uncompleted_value": [],
            "completed_value": [],
            "top_contracts": []
        }
        # Filter by comodity, spending type
        eligible_contracts = []
        all_commodity = Contract_commodity_group.objects.all()
        for comodity in all_commodity:
            comodity_name = comodity.name
            qs = comodity.contract_tracking_set.all()
            if request.method == "POST":
                qs = filterQs(qs, post_data)
            eligible_contracts = eligible_contracts + \
                (list(qs.order_by('-nzd')[:10]))
            number_of_contracts = qs.all().count()
            completed = qs.filter(
                completed_count=True).count()
            uncompleted = qs.filter(
                completed_count=False).count()
            completed_value = qs.filter(completed_count=True).aggregate(
                total_value=Sum('nzd'))["total_value"]
            uncompleted_value = qs.filter(completed_count=False).aggregate(
                total_value=Sum('nzd'))["total_value"]

            data["comodity_name"].append(comodity_name)
            data["number_of_contracts"].append(number_of_contracts)
            data["uncompleted"].append(uncompleted)
            data["completed"].append(completed)
            data["uncompleted_value"].append(uncompleted_value)
            data["completed_value"].append(completed_value)

        eligible_contracts = [
            item for item in eligible_contracts if item.nzd != None]
        eligible_contracts.sort(key=lambda x: x.nzd, reverse=True)
        eligible_contracts = eligible_contracts[:10]

        counter = 1
        for item in eligible_contracts:
            data["top_contracts"].append([
                counter, item.supplier, item.expiry_date, item.commodity_group.name, renderRupiah(
                    item.nzd), item.remarks_this_week
            ])
            counter += 1
        return JsonResponse(data)


def api_target_actual(request):
    if request.method in ["POST", "GET"]:
        if request.body != b'':
            post_data = json.loads(request.body)
        data = {
            "quarter_label": ["Q1", "Q2", "Q3", "Q4"],
            "month_label": ['AUG', 'SEP', 'OCT', 'NOV', 'DEC', 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL'],
            "target_monthly": [],
            "actual_monthly": [],
            "target_qurterly": [],
            "actual_qurterly": []
        }

        # process target
        last_month_total = 0
        counter = 0
        # insert filter buyer, commodity and direct here
        target_tracking = filterQs(Contract_target.objects.all(), post_data)

        for month in MONTH_FY:
            counter += 1
            for track in target_tracking.filter(month__month=month):
                last_month_total += track.value

            data["target_monthly"].append(last_month_total)

            if counter == 3:
                counter = 0
                data["target_qurterly"].append(last_month_total)

        total_target = data["target_monthly"][-1]
        if total_target != 0:
            data["target_monthly"] = [
                round(value/total_target, 3) for value in data["target_monthly"]]
            data["target_qurterly"] = [
                round(value/total_target, 3) for value in data["target_qurterly"]]

        # process actual
        last_month_total = 0
        counter = 0
        # insert filter buyer, commodity and direct here
        target_tracking = filterQs(Contract_actual.objects.all(), post_data)

        for month in MONTH_FY:
            counter += 1
            for track in target_tracking.filter(month__month=month):
                last_month_total += track.value

            data["actual_monthly"].append(last_month_total)

            if counter == 3:
                counter = 0
                data["actual_qurterly"].append(last_month_total)

        if total_target != 0:
            data["actual_monthly"] = [
                round(value/total_target, 3) for value in data["actual_monthly"]]
            data["actual_qurterly"] = [
                round(value/total_target, 3) for value in data["actual_qurterly"]]

        return JsonResponse(data)


def api_venus_open_po(request):
    data = {
        "month_label": ['AUG', 'SEP', 'OCT', 'NOV', 'DEC', 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL'],
    }

    value_closed_item_arr = []
    value_open_item_arr = []
    count_closed_item_arr = []
    count_open_item_arr = []
    top_item_arr = []

    qs = Venus_open_po.objects.all()
    filter_param = pasrseVenusFilterParam(request)
    qs = filterQsVenus(qs, filter_param)

    for month in MONTH_FY:
        closed_item = qs.filter(month__month=month, status__icontains="yes")
        value_closed_item_arr.append(closed_item.aggregate(
            total_value=Sum('amount'))["total_value"])
        count_closed_item_arr.append(closed_item.count())

        open_item = qs.filter(month__month=month, status__icontains="no")
        value_open_item_arr.append(open_item.aggregate(
            total_value=Sum('amount'))["total_value"])
        top_item_arr = top_item_arr + \
            (list(open_item.order_by('-amount')[:10]))
        count_open_item_arr.append(open_item.count())

    top_item_arr = [
        item for item in top_item_arr if item.amount != None]
    top_item_arr.sort(key=lambda x: x.amount, reverse=True)
    top_item_arr = top_item_arr[:10]

    # Days calculation
    all_user_monthly_average = []
    for user in Venus_department.objects.all():
        user_monthly_days = []
        for month in MONTH_FY:
            monthly_user_cases = qs.filter(
                department=user, month__month=month, status__icontains="no")
            if monthly_user_cases.count() > 0:
                monthly_user_days = monthly_user_cases.aggregate(
                    total_days=Sum("po_age"))["total_days"]
                user_monthly_days.append(monthly_user_days)
        if len(user_monthly_days) == 0:
            all_user_monthly_average.append(0)
        else:
            user_yearly_avg = sum(user_monthly_days)/len(user_monthly_days)
            all_user_monthly_average.append(user_yearly_avg)
    if len(all_user_monthly_average) == 0:
        all_user_yearly_avg = 0
    else:
        all_user_yearly_avg = sum(
            all_user_monthly_average)/len(all_user_monthly_average)

    data["value_closed_item"] = [item if item !=
                                 None else 0 for item in value_closed_item_arr]
    data["value_open_item"] = [item if item !=
                               None else 0 for item in value_open_item_arr]
    data["count_closed_item"] = [item if item !=
                                 None else 0 for item in count_closed_item_arr]
    data["count_open_item"] = [item if item !=
                               None else 0 for item in count_open_item_arr]
    data["top_item"] = [(item.department.name, item.po_number, item.po_age, renderRupiah(
        item.amount), item.pic, item.remarks) for item in top_item_arr]
    data["all_user_yearly_avg"] = all_user_yearly_avg

    return JsonResponse(data)


def api_venus_grni(request):
    data = {
        "month_label": ['AUG', 'SEP', 'OCT', 'NOV', 'DEC', 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL'],
    }

    value_closed_item_arr = []
    value_open_item_arr = []
    count_closed_item_arr = []
    count_open_item_arr = []
    top_item_arr = []

    qs = Venus_grni.objects.all()
    filter_param = pasrseVenusFilterParam(request)
    qs = filterQsVenus(qs, filter_param)
    # Insert filtering here
    for month in MONTH_FY:
        closed_item = qs.filter(month__month=month, status__icontains="yes")
        value_closed_item_arr.append(closed_item.aggregate(
            total_value=Sum('amount'))["total_value"])
        count_closed_item_arr.append(closed_item.count())

        open_item = qs.filter(month__month=month, status__icontains="no")
        value_open_item_arr.append(open_item.aggregate(
            total_value=Sum('amount'))["total_value"])
        top_item_arr = top_item_arr + \
            (list(open_item.order_by('-amount')[:10]))
        count_open_item_arr.append(open_item.count())

    top_item_arr = [
        item for item in top_item_arr if item.amount != None]
    top_item_arr.sort(key=lambda x: x.amount, reverse=True)
    top_item_arr = top_item_arr[:10]

    # Days calculation
    all_user_monthly_average = []
    for user in Venus_department.objects.all():
        user_monthly_days = []
        for month in MONTH_FY:
            monthly_user_cases = qs.filter(
                department=user, month__month=month, status__icontains="no")
            if monthly_user_cases.count() > 0:
                monthly_user_days = monthly_user_cases.aggregate(
                    total_days=Sum("po_age"))["total_days"]
                user_monthly_days.append(monthly_user_days)
        if len(user_monthly_days) == 0:
            all_user_monthly_average.append(0)
        else:
            user_yearly_avg = sum(user_monthly_days)/len(user_monthly_days)
            all_user_monthly_average.append(user_yearly_avg)
    if len(all_user_monthly_average) == 0:
        all_user_yearly_avg = 0
    else:
        all_user_yearly_avg = sum(
            all_user_monthly_average)/len(all_user_monthly_average)

    data["value_closed_item"] = [-item if item !=
                                 None else 0 for item in value_closed_item_arr]
    data["value_open_item"] = [-item if item !=
                               None else 0 for item in value_open_item_arr]
    data["count_closed_item"] = [item if item !=
                                 None else 0 for item in count_closed_item_arr]
    data["count_open_item"] = [item if item !=
                               None else 0 for item in count_open_item_arr]
    data["top_item"] = [(item.department.name, item.po_number, item.po_age, renderRupiah(
        item.amount), item.pic, item.remarks) for item in top_item_arr]
    data["all_user_yearly_avg"] = all_user_yearly_avg

    return JsonResponse(data)


def api_venus_po_after(request):
    data = {
        "month_label": ['AUG', 'SEP', 'OCT', 'NOV', 'DEC', 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL'],
    }

    value_closed_item_arr = []
    value_open_item_arr = []
    count_closed_item_arr = []
    count_open_item_arr = []
    top_item_arr = []

    qs = Venus_po_after.objects.all()
    filter_param = pasrseVenusFilterParam(request)
    qs = filterQsVenus(qs, filter_param)
    # Insert filtering here
    for month in MONTH_FY:
        closed_item = qs.filter(month__month=month, status__icontains="yes")
        value_closed_item_arr.append(closed_item.aggregate(
            total_value=Sum('amount'))["total_value"])
        count_closed_item_arr.append(closed_item.count())

        open_item = qs.filter(month__month=month, status__icontains="no")
        value_open_item_arr.append(open_item.aggregate(
            total_value=Sum('amount'))["total_value"])
        top_item_arr = top_item_arr + \
            (list(open_item.order_by('-amount')[:10]))
        count_open_item_arr.append(open_item.count())

    top_item_arr = [
        item for item in top_item_arr if item.amount != None]
    top_item_arr.sort(key=lambda x: x.amount, reverse=True)
    top_item_arr = top_item_arr[:10]

    # Days calculation
    all_user_monthly_average = []
    for user in Venus_department.objects.all():
        user_monthly_days = []
        for month in MONTH_FY:
            monthly_user_cases = qs.filter(
                department=user, month__month=month, status__icontains="no")
            if monthly_user_cases.count() > 0:
                monthly_user_days = monthly_user_cases.aggregate(
                    total_days=Sum("po_age"))["total_days"]
                user_monthly_days.append(monthly_user_days)
        if len(user_monthly_days) == 0:
            all_user_monthly_average.append(0)
        else:
            user_yearly_avg = sum(user_monthly_days)/len(user_monthly_days)
            all_user_monthly_average.append(user_yearly_avg)
    if len(all_user_monthly_average) == 0:
        all_user_yearly_avg = 0
    else:
        all_user_yearly_avg = sum(
            all_user_monthly_average)/len(all_user_monthly_average)

    data["value_closed_item"] = [item if item !=
                                 None else 0 for item in value_closed_item_arr]
    data["value_open_item"] = [item if item !=
                               None else 0 for item in value_open_item_arr]
    data["count_closed_item"] = [item if item !=
                                 None else 0 for item in count_closed_item_arr]
    data["count_open_item"] = [item if item !=
                               None else 0 for item in count_open_item_arr]
    data["top_item"] = [(item.department.name, item.po_number, item.po_age, renderRupiah(
        item.amount), item.pic, item.remarks) for item in top_item_arr]
    data["all_user_yearly_avg"] = all_user_yearly_avg

    return JsonResponse(data)


def venus_grni_export_csv(request):
    qs = Venus_grni_raw.objects.filter(doc_type__in=["OV", "PV"])

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="export_GRNI.csv"'

    writer = csv.writer(response)
    writer.writerow(['Po Type', 'Department', 'Vendor', 'Business Unit', 'Po Number',
                     'Po Age', 'Status', 'Month', 'Amount', 'Pic', 'Remarks', "Doc Type"])
    for item in qs:
        status = "Yes" if item.pair != None else "No"
        writer.writerow([item.po_type, None, None, None, item.po_number,
                         None, status, None, item.amount, None, item.remark, item.doc_type])

    return response


def venus_grni_export_xlsx(request):
    qs = Venus_grni_raw.objects.filter(doc_type__in=["OV", "PV"])

    header = ['System ID', 'Po Type', 'Department', 'Vendor', 'Business Unit', 'Po Number',
              'Po Age', 'Status', 'Month', 'Amount', 'Pic', 'Remarks', "Doc Type", "Pair ID"]

    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    )
    response['Content-Disposition'] = 'attachment; filename=export_GRNI.xlsx'
    workbook = openpyxl.Workbook()

    worksheet = workbook.active
    worksheet.title = 'GRNI'

    row_num = 1

    # Assign the titles for each cell of the header
    for col_num, column_title in enumerate(header, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title

    # Iterate through all movies
    for item in qs:
        row_num += 1

        # Define the data for each cell in the row
        status = "Yes" if item.pair != None else "No"
        pair = item.pair.id if item.pair != None else None
        row = [item.id, item.po_type, None, None, None, item.po_number, None,
               status, None, item.amount, None, item.remark, item.doc_type, pair]

        # Assign the data for each cell of the row
        for col_num, cell_value in enumerate(row, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            cell.value = cell_value

    workbook.save(response)

    return response


class api_venus_grni_table(BaseDatatableView):
    model = Venus_grni_raw
    columns = ['id', 'po_type', 'po_number', 'doc_type', 'doc_number', 'doc_date',
               'supplier_code', 'supplier_name', 'remark', 'po_currency', 'amount', 'unit']
    order_columns = columns

    def filter_queryset(self, qs):
        filter_param = pasrseVenusFilterParamRaw(self.request)
        qs = qs.filter(archived = False, doc_type__in = ["PV", "OV"])
        qs = super().filter_queryset(qs)
        qs = filterQsVenusRaw(qs, filter_param)
        return qs


class api_venus_opo_table(BaseDatatableView):
    model = Venus_opo_raw
    columns = ['id', 'order_number', 'or_ty', 'amount_to_receive', 'ordered_amount', 'order_co', 'supplier_number', 'supplier_name', 'line_description', 'last_status', 'next_status', 'gl_date', 'order_date', 'quantity_to_receive',
               'unit_cost', 'original_order_no', 'orig_ord_type', 'account_number', 'cost_center', 'obj_acct', 'line_number', 'managrl_code_1', 'managrl_code_2', 'managrl_code_3', 'managrl_code_4', 'second_item_number']
    order_columns = columns

    def filter_queryset(self, qs):
        qs = super().filter_queryset(qs)
        qs = qs.filter(archived = False)
        filter_param = pasrseVenusFilterParamRaw(self.request)
        if filter_param.get('po_type', None) != None:
            qs = qs.filter(or_ty__in=filter_param.get('po_type'))
        return qs


class api_venus_poa_table(BaseDatatableView):
    model = Venus_poa_raw
    columns = ['id', 'company', 'payment_ref', 'voucher_gl_date', 'supplier_invoice_number', 'invoice_date', 'payment_gl_date', 'voucher_doc_type', 'voucher_number', 'po_number', 'po_type', 'po_date', 'po_description_line_1', 'po_description_line_2',
               'vendor_code', 'vendor_name', 'item_code', 'item_description', 'stocking_type', 'stocking_type_description', 'account_code', 'account_description', 'business_unit', 'amount', 'domestic_currency_code', 'currency_code', 'search_type']
    order_columns = columns

    def filter_queryset(self, qs):
        qs = super().filter_queryset(qs)
        qs = qs.filter(archived = False)
        filter_param = pasrseVenusFilterParamRaw(self.request)
        if filter_param.get('po_type', None) != None:
            qs = qs.filter(po_type__in=filter_param.get('po_type'))
        return qs


def filterQs(querySet, param):
    qs = querySet
    if param.get('buyer', "all") != "all":
        buyer_id = int(param.get('buyer'))
        qs = qs.filter(buyer=buyer_id)

    if param.get('spending', "all") != "all":
        direct = True if param.get('spending') == "direct" else False
        qs = qs.filter(direct=direct)

    if param.get('commodity', "all") != "all":
        commodity_id = int(param.get('commodity'))
        qs = qs.filter(commodity_group=commodity_id)

    return qs


def editGRNI(request, id):
    if request.method == "POST":
        instance = get_object_or_404(Venus_grni_raw, pk=id)
        form = RawGRNIForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save()
            return HttpResponseRedirect("/venus-table")
        else:
            context = {
                "form": form,
                "url": request.path,
                "title": "GRNI"
            }
            return render(request, "pages/edit-table-row-venus.html", context)
    else:
        instance = get_object_or_404(Venus_grni_raw, pk=id)
        form = RawGRNIForm(instance=instance)
        context = {
            "form": form,
            "url": request.path,
            "title": "GRNI"
        }
        return render(request, "pages/edit-table-row-venus.html", context)


def editOPO(request, id):
    if request.method == "POST":
        instance = get_object_or_404(Venus_opo_raw, pk=id)
        form = RawOPOForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save()
            return HttpResponseRedirect("/venus-table")
        else:
            context = {
                "form": form,
                "url": request.path,
                "title": "Open PO"
            }
            return render(request, "pages/edit-table-row-venus.html", context)
    else:
        instance = get_object_or_404(Venus_opo_raw, pk=id)
        form = RawOPOForm(instance=instance)
        context = {
            "form": form,
            "url": request.path,
            "title": "Open PO"
        }
        return render(request, "pages/edit-table-row-venus.html", context)


def editPOA(request, id):
    if request.method == "POST":
        instance = get_object_or_404(Venus_poa_raw, pk=id)
        form = RawPOAForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save()
            return HttpResponseRedirect("/venus-table")
        else:
            context = {
                "form": form,
                "url": request.path,
                "title": "PO After Invoice"
            }
            return render(request, "pages/edit-table-row-venus.html", context)
    else:
        instance = get_object_or_404(Venus_poa_raw, pk=id)
        form = RawPOAForm(instance=instance)
        context = {
            "form": form,
            "url": request.path,
            "title": "PO After Invoice"
        }
        return render(request, "pages/edit-table-row-venus.html", context)


def createGRNI(request):
    if request.method == "POST":
        form = RawGRNIForm(request.POST)
        if form.is_valid():
            obj = form.save()
            return HttpResponseRedirect("/venus-table")
        else:
            context = {
                "form": form,
                "url": request.path,
                "title": "Create GRNI"
            }
            return render(request, "pages/edit-table-row-venus.html", context)
    else:
        form = RawGRNIForm()
        context = {
            "form": form,
            "url": request.path,
            "title": "Create GRNI"
        }
        return render(request, "pages/edit-table-row-venus.html", context)


def createOPO(request):
    if request.method == "POST":
        form = RawOPOForm(request.POST)
        if form.is_valid():
            obj = form.save()
            return HttpResponseRedirect("/venus-table")
        else:
            context = {
                "form": form,
                "url": request.path,
                "title": "Create Open PO"
            }
            return render(request, "pages/edit-table-row-venus.html", context)
    else:
        form = RawOPOForm()
        context = {
            "form": form,
            "url": request.path,
            "title": "Create Open PO"
        }
        return render(request, "pages/edit-table-row-venus.html", context)


def createPOA(request):
    if request.method == "POST":
        form = RawPOAForm(request.POST)
        if form.is_valid():
            obj = form.save()
            return HttpResponseRedirect("/venus-table")
        else:
            context = {
                "form": form,
                "url": request.path,
                "title": "Create PO After Invoice"
            }
            return render(request, "pages/edit-table-row-venus.html", context)
    else:
        form = RawPOAForm()
        context = {
            "form": form,
            "url": request.path,
            "title": "Create PO After Invoice"
        }
        return render(request, "pages/edit-table-row-venus.html", context)


def deleteGRNI(request, id):
    instance = get_object_or_404(Venus_grni_raw, pk=id)
    instance.archived = True
    instance.save()
    return HttpResponseRedirect("/venus-table")


def deleteOPO(request, id):
    instance = get_object_or_404(Venus_opo_raw, pk=id)
    instance.archived = True
    instance.save()
    return HttpResponseRedirect("/venus-table")


def deletePOA(request, id):
    instance = get_object_or_404(Venus_poa_raw, pk=id)
    instance.archived = True
    instance.save()
    return HttpResponseRedirect("/venus-table")


def restoreGRNI(request, id):
    instance = get_object_or_404(Venus_grni_raw, pk=id)
    instance.archived = False
    instance.save()
    return HttpResponseRedirect("/venus-table")


def restoreOPO(request, id):
    instance = get_object_or_404(Venus_opo_raw, pk=id)
    instance.archived = False
    instance.save()
    return HttpResponseRedirect("/venus-table")


def restorePOA(request, id):
    instance = get_object_or_404(Venus_poa_raw, pk=id)
    instance.archived = False
    instance.save()
    return HttpResponseRedirect("/venus-table")


def filterQsVenus(querySet, param):
    qs = querySet

    # if param.get('commodity', None) != None:
    #     qs = qs.filter(business_unit__in = param.get('commodity'))

    # if param.get('po_type', None) != None:
    #     qs = qs.filter(po_type__in = param.get('po_type'))

    # if param.get('user', None) != None:
    #     qs = qs.filter(department__in = param.get('user'))

    if param.get('commodity', None) != None:
        qs = qs.filter(business_unit__name__in=param.get('commodity'))

    if param.get('po_type', None) != None:
        qs = qs.filter(po_type__name__in=param.get('po_type'))

    if param.get('user', None) != None:
        qs = qs.filter(department__name__in=param.get('user'))

    return qs


def filterQsVenusRaw(querySet, param):
    qs = querySet

    if param.get('po_type', None) != None:
        qs = qs.filter(po_type__in=param.get('po_type'))

    return qs


def pasrseVenusFilterParam(request):
    # data = {
    #     "commodity": [int(item) for item in request.GET.getlist('commodity', [])],
    #     "po_type": [int(item) for item in request.GET.getlist('po_type', [])],
    #     "user": [int(item) for item in request.GET.getlist('user', [])]
    # }
    data = {
        "commodity": request.GET.getlist('commodity', []),
        "po_type": request.GET.getlist('po_type', []),
        "user": request.GET.getlist('user', [])
    }
    return data


def pasrseVenusFilterParamRaw(request):
    data = request.GET.get('filterParam', {})
    data = parse_qs(data)
    return data
