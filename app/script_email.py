import psycopg2
import smtplib, ssl
from datetime import datetime


def db_print() :
    try:
        connection = psycopg2.connect(user="email_test_user",
                                        password="dbpassword",
                                        host="127.0.0.1",
                                        port="5432",
                                        database="email_test")
        cursor = connection.cursor()
        postgreSQL_select_Query = "select * from tb_master where status = 'Open' "

        cursor.execute(postgreSQL_select_Query)
        print("Selecting rows from mobile table using cursor.fetchall")
        table_records = cursor.fetchall() 

        print("Print each row and it's columns values")
        for row in table_records:
            
            print("PO_Number = ", row[0], )
            print("PIC = ", row[1])
            print("Status = ", row[2])
            print("Ammount  = ", row[3], "\n")
            

    except (Exception, psycopg2.Error) as error :
        print ("Error while fetching data from PostgreSQL", error)



    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")


# print(str(db_print()))

def db_str() :
    try:
        connection = psycopg2.connect(user="email_test_user",
                                        password="dbpassword",
                                        host="127.0.0.1",
                                        port="5432",
                                        database="email_test")
        cursor = connection.cursor()
        postgreSQL_select_Query = "select * from tb_master where status = 'Open' "

        cursor.execute(postgreSQL_select_Query)
        print("Selecting rows from mobile table using cursor.fetchall")
        table_records = cursor.fetchall() 
        
        print("Print each row and it's columns values")

        po_number = []
        pic = []
        status = []
        ammount = []



        for row in table_records:
            
            po_number.append(row[0])
            pic.append(row[1])
            status.append(row[2])
            ammount.append(row[3])
            
        info = ""

        # presidents = ["Washington", "Adams", "Jefferson", "Madison", "Monroe", "Adams", "Jackson"]
        # for i in range(len(presidents)):
        #     print("President {}: {}".format(i + 1, presidents[i]))
        
        for i in range(len(po_number)):
            info += "PO Number = " + str(po_number[i]) + "\n" + "PIC = " + str(pic[i]) + "\n" + "Status = " + str(status[i]) + "\n" "Ammount = "+ str(ammount[i]) + "\n" + "\n"

    except (Exception, psycopg2.Error) as error :
        print ("Error while fetching data from PostgreSQL", error)



    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            return(info)
            # print("PostgreSQL connection is closed")

# print(db_str())

port = 465  # For SSL
smtp_server = "mail.kotadigivice.com"
sender_email = "aufar@kotadigivice.com"  # Enter your address
receiver_email = "aufarrizki@gmail.com"  # Enter receiver address
password = input("Type your password and press enter: ")
message = "Please update on reasons of being left open, thank you. " + "\n" + "\n" + str(db_str())
context = ssl.create_default_context()
with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
    server.login(sender_email, password)
    server.sendmail(sender_email, receiver_email, message)