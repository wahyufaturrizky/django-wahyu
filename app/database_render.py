from app.models import *
from app.utils import *

import datetime

def insertContractDatatoDB(datas):
    commodity = list(set(column(datas, 3)))
    insertCommodityGrouptoTable(commodity)
    supply = list(set(column(datas, 4)))
    insertSupplyCategorytoTable(supply)
    buyer = list(set(column(datas, 8)))
    insertBuyertoTable(buyer)
    period = list(set(column(datas, 9)))
    insertPeriodtoTable(period)

    for row in datas:
        commodity_foreignkey = Contract_commodity_group.objects.get(name=row[3])
        supply_foreignkey = Contract_supply_category.objects.get(name=row[4])
        buyer_foreignkey = Contract_buyer.objects.get(name=row[8])
        period_foreignkey = Contract_period_coverage.objects.get(period=row[9])
        print(row[5])
        tracking = Contract_tracking(
            supplier = row[0],
            supplier_ID = row[1],
            direct = row[2] == 'Direct',
            commodity_group = commodity_foreignkey,
            supply_category = supply_foreignkey,
            nzd = row[5],
            contract_ID = row[6],
            expiry_date = row[7],
            buyer = buyer_foreignkey,
            period_coverage = period_foreignkey,
            completed_count = row[10] == 'Completed',
            remarks_last_week = row[59],
            remarks_this_week = row[60],
        )

        tracking.save()

        quarter = Contract_quarter(
            tracking = tracking,
            quarter = 1,
            week1 = row [11],
            week2 = row [12],
            week3 = row [13],
            week4 = row [14],
            week5 = row [15],
            week6 = row [16],
            week7 = row [17],
            week8 = row [18],
            week9 = row [19],
            week10 = row [20],
            week11 = row [21],
            week12 = row [22],
        )

        quarter.save()

        quarter = Contract_quarter(
            tracking = tracking,
            quarter = 2,
            week1 = row [23],
            week2 = row [24],
            week3 = row [25],
            week4 = row [26],
            week5 = row [27],
            week6 = row [28],
            week7 = row [29],
            week8 = row [30],
            week9 = row [31],
            week10 = row [32],
            week11 = row [33],
            week12 = row [34],
        )

        quarter.save()

        quarter = Contract_quarter(
            tracking = tracking,
            quarter = 3,
            week1 = row [35],
            week2 = row [36],
            week3 = row [37],
            week4 = row [38],
            week5 = row [39],
            week6 = row [40],
            week7 = row [41],
            week8 = row [42],
            week9 = row [43],
            week10 = row [44],
            week11 = row [45],
            week12 = row [46],
        )

        quarter.save()

        quarter = Contract_quarter(
            tracking = tracking,
            quarter = 4,
            week1 = row [47],
            week2 = row [48],
            week3 = row [49],
            week4 = row [50],
            week5 = row [51],
            week6 = row [52],
            week7 = row [53],
            week8 = row [54],
            week9 = row [55],
            week10 = row [56],
            week11 = row [57],
            week12 = row [58],
        )

        quarter.save()

def insertCommodityGrouptoTable(datas):
    for name in datas:
        commodity_group = Contract_commodity_group(
            name = name,
        )
        commodity_group.save()

def insertSupplyCategorytoTable(datas):
    for name in datas:
        supply_category = Contract_supply_category(
            name = name,
        )
        supply_category.save()

def insertBuyertoTable(datas):
    for name in datas:
        buyer = Contract_buyer(
            name = name,
        )
        buyer.save()

def insertPeriodtoTable(datas):
    for data in datas:
        period_coverage = Contract_period_coverage(
            period = data,
        )
        period_coverage.save()

def insertTargetDatatoDB(target_data):
    for row in target_data:
        commodity_foreignkey = Contract_commodity_group.objects.get(name__iexact=row[2])
        buyer_foreignkey = Contract_buyer.objects.get(name=row[0])

        target = Contract_target(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2019, 8, 1),
            value = row[3],
        )
        
        target.save()

        target = Contract_target(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2019, 9, 1),
            value = row[4],
        )
        
        target.save()

        target = Contract_target(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2019, 10, 1),
            value = row[5],
        )
        
        target.save()

        target = Contract_target(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2019, 11, 1),
            value = row[6],
        )

        target.save()

        target = Contract_target(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2019, 12, 1),
            value = row[7],
        )

        target.save()

        target = Contract_target(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 1, 1),
            value = row[8],
        )

        target.save()

        target = Contract_target(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 2, 1),
            value = row[9],
        )

        target.save()

        target = Contract_target(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 3, 1),
            value = row[10],
        )

        target.save()

        target = Contract_target(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 4, 1),
            value = row[11],
        )

        target.save()

        target = Contract_target(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 5, 1),
            value = row[12],
        )

        target.save()

        target = Contract_target(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 6, 1),
            value = row[13],
        )

        target.save()

        target = Contract_target(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 7, 1),
            value = row[14],
        )

        target.save()

def insertActualDatatoDB(actual_data):
    for row in actual_data:
        commodity_foreignkey = Contract_commodity_group.objects.get(name__iexact=row[2])
        buyer_foreignkey = Contract_buyer.objects.get(name=row[0])

        actual = Contract_actual(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2019, 8, 1),
            value = row[3],
        )
        
        actual.save()

        actual = Contract_actual(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2019, 9, 1),
            value = row[4],
        )
        
        actual.save()

        actual = Contract_actual(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2019, 10, 1),
            value = row[5],
        )
        
        actual.save()

        actual = Contract_actual(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2019, 11, 1),
            value = row[6],
        )

        actual.save()

        actual = Contract_actual(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2019, 12, 1),
            value = row[7],
        )

        actual.save()

        actual = Contract_actual(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 1, 1),
            value = row[8],
        )

        actual.save()

        actual = Contract_actual(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 2, 1),
            value = row[9],
        )

        actual.save()

        actual = Contract_actual(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 3, 1),
            value = row[10],
        )

        actual.save()

        actual = Contract_actual(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 4, 1),
            value = row[11],
        )

        actual.save()

        actual = Contract_actual(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 5, 1),
            value = row[12],
        )

        actual.save()

        actual = Contract_actual(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 6, 1),
            value = row[13],
        )

        actual.save()

        actual = Contract_actual(
            buyer = buyer_foreignkey,
            commodity_group = commodity_foreignkey,
            direct = row[1] == 'Direct',
            month = datetime.date(2020, 7, 1),
            value = row[14],
        )

        actual.save()

def insertVenusDatatoDB(open_po, grni, po_after):
    po_type = list(set(column(open_po, 1) + column(grni, 0) + column(po_after, 5)))
    insertOpTypetoTable(po_type)
    departments = list(set(column(open_po, 20) + column(grni, 20) + column(po_after, 14)))
    insertDepartmentstoTable(departments)
    vendor_name = list(set(column(open_po, 8) + column(grni, 7) + column(po_after, 8)))
    insertVendortoTable(vendor_name)
    insertBusinessUnit()
    insertOpenPotoTable(open_po)
    insertGrnitoTable(grni)
    insertPoAftertoTable(po_after)

def insertOpenPotoTable(open_po):
    for row in open_po:
        po_type_foreignkey = Venus_po_type.objects.get(name=row[1])
        department_foreignkey = Venus_department.objects.get(name=row[20])
        vendor_foreignkey = Venus_vendor.objects.get(name=row[8])
        business_unit_foreignkey = Venus_business_unit.objects.get(code=row[5])

        venus_open_po = Venus_open_po(
            po_type = po_type_foreignkey,
            department = department_foreignkey,
            vendor = vendor_foreignkey,
            business_unit = business_unit_foreignkey,
            po_number = row[0],
            po_age = row[10],
            status = str(row[22]).lower(),
            month = row[24],
            amount = row[18],
            pic = row[21],
            remarks = row[23],
        )

        venus_open_po.save()

def insertGrnitoTable(grni):
    for row in grni:
        po_type_foreignkey = Venus_po_type.objects.get(name=row[0])
        department_foreignkey = Venus_department.objects.get(name=row[20])
        vendor_foreignkey = Venus_vendor.objects.get(name=row[7])
        business_unit_foreignkey = Venus_business_unit.objects.get(code=row[19])

        venus_grni = Venus_grni(
            po_type = po_type_foreignkey,
            department = department_foreignkey,
            vendor = vendor_foreignkey,
            business_unit = business_unit_foreignkey,
            po_number = row[1],
            po_age = row[12],
            status = str(row[16]).lower(),
            month = row[18],
            amount = row[10],
            pic = row[14],
            remarks = row[17],
        )

        venus_grni.save()

def insertPoAftertoTable(po_after):
    for row in po_after:
        po_type_foreignkey = Venus_po_type.objects.get(name=row[5])
        department_foreignkey = Venus_department.objects.get(name=row[14])
        vendor_foreignkey = Venus_vendor.objects.get(name=row[8])
        business_unit_foreignkey = Venus_business_unit.objects.get(code=row[16])

        venus_po_after = Venus_po_after(
            po_type = po_type_foreignkey,
            department = department_foreignkey,
            vendor = vendor_foreignkey,
            business_unit = business_unit_foreignkey,
            po_number = row[4],
            po_age = row[12],
            status = str(row[18]).lower(),
            month = row[17],
            amount = row[9],
            pic = row[13],
            remarks = row[19],
        )

        venus_po_after.save()

def insertOpTypetoTable(datas):
    for name in datas:
        po_type = Venus_po_type(
            name = name,
        )
        po_type.save()

def insertDepartmentstoTable(datas):
    for name in datas:
        department = Venus_department(
            name = name,
        )
        department.save()

def insertVendortoTable(vendor_name):
    for name in vendor_name:
        vendor = Venus_vendor(
            name = name,
        )
        vendor.save()

def insertBusinessUnit():
    unit = Venus_business_unit(
        code = 1193,
        name = "FBI"
    )
    unit.save()
    unit = Venus_business_unit(
        code = 2157,
        name = "FBMI"
    )
    unit.save()