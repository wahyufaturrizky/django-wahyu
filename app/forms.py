from django.forms import ModelForm

from app.models import Venus_grni_raw, Venus_opo_raw, Venus_poa_raw

class RawGRNIForm(ModelForm):
    class Meta:
        model = Venus_grni_raw
        exclude = ["pair", "archived"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class RawOPOForm(ModelForm):
    class Meta:
        model = Venus_opo_raw
        exclude = ["archived"]
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

class RawPOAForm(ModelForm):
    class Meta:
        model = Venus_poa_raw
        exclude = ["archived"]
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'