# Generated by Django 3.0.2 on 2020-01-22 04:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_venus_grni_raw'),
    ]

    operations = [
        migrations.AddField(
            model_name='venus_grni_raw',
            name='po_type',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
