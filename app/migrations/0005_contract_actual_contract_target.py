# Generated by Django 3.0.2 on 2020-01-16 09:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_auto_20200116_0439'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contract_target',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('direct', models.BooleanField(blank=True, null=True)),
                ('month', models.DateField(blank=True, null=True)),
                ('value', models.IntegerField(blank=True, null=True)),
                ('buyer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Contract_buyer')),
                ('commodity_group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Contract_commodity_group')),
            ],
        ),
        migrations.CreateModel(
            name='Contract_actual',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('direct', models.BooleanField(blank=True, null=True)),
                ('month', models.DateField(blank=True, null=True)),
                ('value', models.IntegerField(blank=True, null=True)),
                ('buyer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Contract_buyer')),
                ('commodity_group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Contract_commodity_group')),
            ],
        ),
    ]
