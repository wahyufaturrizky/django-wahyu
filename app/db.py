from django.db import connection

def truncate_contract_db():
    cursor = connection.cursor()
    cursor.execute("truncate table app_contract_period_coverage,app_contract_quarter, app_contract_tracking, app_contract_buyer, app_contract_commodity_group, app_contract_supply_category, app_contract_target, app_contract_actual")

def truncate_venus_db():
    cursor = connection.cursor()
    cursor.execute("truncate table app_venus_open_po, app_venus_grni, app_venus_po_after, app_venus_po_type, app_venus_department, app_venus_vendor, app_venus_business_unit")