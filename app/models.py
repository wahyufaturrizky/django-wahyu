# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.db import models
from django.contrib.auth.models import User

class Contract_commodity_group(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)

class Contract_supply_category(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)

class Contract_buyer(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)

class Contract_period_coverage(models.Model):
    period = models.CharField(max_length=255, blank=True, null=True)

class Contract_tracking(models.Model):
    supplier = models.CharField(max_length=255, blank=True, null=True)
    supplier_ID = models.CharField(max_length=255, blank=True, null=True)
    direct = models.BooleanField(blank=True, null=True)
    commodity_group = models.ForeignKey('Contract_commodity_group', on_delete=models.CASCADE)
    supply_category = models.ForeignKey('Contract_supply_category', on_delete=models.CASCADE)
    nzd = models.FloatField(blank=True, null=True)
    contract_ID = models.CharField(max_length=255, blank=True, null=True)
    expiry_date = models.DateField(blank=True, null=True)
    buyer = models.ForeignKey('Contract_buyer', on_delete=models.CASCADE)
    period_coverage = models.ForeignKey('Contract_period_coverage', on_delete=models.CASCADE)
    completed_count = models.BooleanField(blank=True, null=True)
    remarks_last_week = models.CharField(max_length=255, blank=True, null=True)
    remarks_this_week = models.CharField(max_length=255, blank=True, null=True)

class Contract_quarter(models.Model):
    tracking = models.ForeignKey('Contract_tracking', on_delete=models.CASCADE)
    quarter = models.IntegerField(blank=True, null=True)
    week1 = models.IntegerField(blank=True, null=True)
    week2 = models.IntegerField(blank=True, null=True)
    week3 = models.IntegerField(blank=True, null=True)
    week4 = models.IntegerField(blank=True, null=True)
    week5 = models.IntegerField(blank=True, null=True)
    week6 = models.IntegerField(blank=True, null=True)
    week7 = models.IntegerField(blank=True, null=True)
    week8 = models.IntegerField(blank=True, null=True)
    week9 = models.IntegerField(blank=True, null=True)
    week10 = models.IntegerField(blank=True, null=True)
    week11 = models.IntegerField(blank=True, null=True)
    week12 = models.IntegerField(blank=True, null=True)

class Contract_target(models.Model):
    buyer = models.ForeignKey('Contract_buyer', on_delete=models.CASCADE)
    commodity_group = models.ForeignKey('Contract_commodity_group', on_delete=models.CASCADE)
    direct = models.BooleanField(blank=True, null=True)
    month = models.DateField(blank=True, null=True)
    value = models.IntegerField(blank=True, null=True)

class Contract_actual(models.Model):
    buyer = models.ForeignKey('Contract_buyer', on_delete=models.CASCADE)
    commodity_group = models.ForeignKey('Contract_commodity_group', on_delete=models.CASCADE)
    direct = models.BooleanField(blank=True, null=True)
    month = models.DateField(blank=True, null=True)
    value = models.IntegerField(blank=True, null=True)

class Venus_po_type(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    
class Venus_department(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)

class Venus_vendor(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)

class Venus_business_unit(models.Model):
    code = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)

class Venus_open_po(models.Model):
    po_type = models.ForeignKey('Venus_po_type', on_delete=models.CASCADE)
    department = models.ForeignKey('Venus_department', on_delete=models.CASCADE)
    vendor = models.ForeignKey('Venus_vendor', on_delete=models.CASCADE)
    business_unit = models.ForeignKey('Venus_business_unit', on_delete=models.CASCADE)
    po_number = models.IntegerField(blank=True, null=True)
    po_age = models.IntegerField(blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)
    month = models.DateField(blank=True, null=True)
    amount = models.BigIntegerField(primary_key=True)
    pic = models.CharField(max_length=255, blank=True, null=True)
    remarks = models.CharField(max_length=255, blank=True, null=True)

class Venus_grni(models.Model):
    po_type = models.ForeignKey('Venus_po_type', on_delete=models.CASCADE)
    department = models.ForeignKey('Venus_department', on_delete=models.CASCADE)
    vendor = models.ForeignKey('Venus_vendor', on_delete=models.CASCADE)
    business_unit = models.ForeignKey('Venus_business_unit', on_delete=models.CASCADE)
    po_number = models.IntegerField(blank=True, null=True)
    po_age = models.IntegerField(blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)
    month = models.DateField(blank=True, null=True)
    amount = models.BigIntegerField(primary_key=True)
    pic = models.CharField(max_length=255, blank=True, null=True)
    remarks = models.CharField(max_length=255, blank=True, null=True)

class Venus_po_after(models.Model):
    po_type = models.ForeignKey('Venus_po_type', on_delete=models.CASCADE)
    department = models.ForeignKey('Venus_department', on_delete=models.CASCADE)
    vendor = models.ForeignKey('Venus_vendor', on_delete=models.CASCADE)
    business_unit = models.ForeignKey('Venus_business_unit', on_delete=models.CASCADE)
    po_number = models.IntegerField(blank=True, null=True)
    po_age = models.IntegerField(blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)
    month = models.DateField(blank=True, null=True)
    amount = models.BigIntegerField(primary_key=True)
    pic = models.CharField(max_length=255, blank=True, null=True)
    remarks = models.CharField(max_length=255, blank=True, null=True)

class Venus_opo_raw(models.Model):
    order_number = models.IntegerField(blank=True, null=True)
    or_ty = models.CharField(max_length=255, blank=True, null=True)
    amount_to_receive = models.FloatField(blank=True, null=True)
    ordered_amount = models.FloatField(blank=True, null=True)
    order_co = models.IntegerField(blank=True, null=True)
    supplier_number = models.IntegerField(blank=True, null=True)
    supplier_name = models.CharField(max_length=255, blank=True, null=True)
    line_description = models.CharField(max_length=255, blank=True, null=True)
    last_status = models.IntegerField(blank=True, null=True)
    next_status = models.IntegerField(blank=True, null=True)
    gl_date = models.DateField(blank=True, null=True)
    order_date = models.DateField(blank=True, null=True)
    quantity_to_receive = models.FloatField(blank=True, null=True)
    unit_cost = models.FloatField(blank=True, null=True)
    original_order_no = models.IntegerField(blank=True, null=True)
    orig_ord_type = models.CharField(max_length=255, blank=True, null=True)
    account_number = models.CharField(max_length=255, blank=True, null=True)
    cost_center = models.FloatField(blank=True, null=True)
    obj_acct = models.IntegerField(blank=True, null=True)
    line_number = models.IntegerField(blank=True, null=True)
    managrl_code_1 = models.CharField(max_length=255, blank=True, null=True)
    managrl_code_2 = models.CharField(max_length=255, blank=True, null=True)
    managrl_code_3 = models.CharField(max_length=255, blank=True, null=True)
    managrl_code_4 = models.CharField(max_length=255, blank=True, null=True)
    second_item_number = models.CharField(
        max_length=255, blank=True, null=True)
    archived = models.BooleanField(default=False, blank=False)

class Venus_grni_raw(models.Model):
    po_type = models.CharField(max_length=255, blank=True, null=True)
    po_number = models.IntegerField(blank=True, null=True)
    po_date = models.DateField(blank=True, null=True)
    doc_type = models.CharField(max_length=255, blank=True, null=True)
    doc_number = models.IntegerField(blank=True, null=True)
    doc_date = models.DateField(blank=True, null=True)
    supplier_code = models.CharField(max_length=255, blank=True, null=True)
    supplier_name = models.CharField(max_length=255, blank=True, null=True)
    remark = models.CharField(max_length=255, blank=True, null=True)
    po_currency = models.CharField(max_length=255, blank=True, null=True)
    amount = models.FloatField(blank=True, null=True)
    unit = models.FloatField(blank=True, null=True)
    pair = models.ForeignKey('Venus_grni_raw', on_delete=models.CASCADE, null=True)
    archived = models.BooleanField(default=False, blank=False)
    
class Venus_poa_raw(models.Model):
    company = models.IntegerField(blank=True, null=True)
    payment_ref =models.IntegerField(blank=True, null=True)
    voucher_gl_date = models.DateField(blank=True, null=True)
    supplier_invoice_number = models.CharField(max_length=255, blank=True, null=True)
    invoice_date = models.DateField(blank=True, null=True)
    payment_gl_date = models.CharField(max_length=255, blank=True, null=True)
    voucher_doc_type = models.CharField(max_length=255, blank=True, null=True)
    voucher_number =models.IntegerField(blank=True, null=True)
    po_number =models.IntegerField(blank=True, null=True)
    po_type = models.CharField(max_length=255, blank=True, null=True)
    po_date = models.DateField(blank=True, null=True)
    po_description_line_1 = models.CharField(max_length=255, blank=True, null=True)
    po_description_line_2 = models.CharField(max_length=255, blank=True, null=True)
    vendor_code =models.IntegerField(blank=True, null=True)
    vendor_name = models.CharField(max_length=255, blank=True, null=True)
    item_code = models.CharField(max_length=255, blank=True, null=True)
    item_description = models.CharField(max_length=255, blank=True, null=True)
    stocking_type = models.CharField(max_length=255, blank=True, null=True)
    stocking_type_description = models.CharField(max_length=255, blank=True, null=True)
    account_code = models.CharField(max_length=255, blank=True, null=True)
    account_description = models.CharField(max_length=255, blank=True, null=True)
    business_unit =models.IntegerField(blank=True, null=True)
    amount =models.FloatField(blank=True, null=True)
    domestic_currency_code = models.CharField(max_length=255, blank=True, null=True)
    currency_code = models.CharField(max_length=255, blank=True, null=True)
    search_type = models.CharField(max_length=255, blank=True, null=True)
    archived = models.BooleanField(default=False, blank=False)