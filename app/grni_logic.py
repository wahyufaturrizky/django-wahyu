from django.db.models import Func, F
from datetime import datetime


def procData(qs):
    start_time = datetime.now()
    margin = 0.02
    paired_ov = []
    paired_pv = []
    keep_id = []
    PVs = qs.filter(doc_type="PV")
    OVs = qs.filter(doc_type="OV")
    total_pv = PVs.count()
    total_ov = OVs.count()

    for pv in PVs:
        max_amount = pv.amount * (1 + margin)
        min_amount = pv.amount * (1 - margin)

        possible_match = OVs.filter(po_number=pv.po_number, po_date=pv.po_date, supplier_code=pv.supplier_code, pair__isnull=True)
        
        possible_match = possible_match.annotate(abs_ammount=Func(F('amount'), function='ABS')).order_by('-abs_ammount') 
        possible_match = possible_match.filter(abs_ammount__gte=min_amount, abs_ammount__lte=max_amount)
        possible_match = possible_match.annotate(division=F('amount') / pv.amount).filter(division__lt=0)

        matching_ov = possible_match.first()
        if matching_ov != None:

            pv.pair = matching_ov
            matching_ov.pair = pv

            pv.save()
            matching_ov.save()
    
    print(f'PVs processed : {total_pv}')
    print(f'OVs processed : {total_ov}')
    print(f'PVs paired : {PVs.filter(pair__isnull=False).count()}')
    print(f'Ovs paired : {OVs.filter(pair__isnull=False).count()}')
    print(f'PVs unpaired : {PVs.filter(pair__isnull=True).count()}')
    print(f'OVs unpaired : {OVs.filter(pair__isnull=True).count()}')
    print((datetime.now() - start_time).seconds)
