FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir /hera
WORKDIR /hera
ADD requirements.txt /hera/
RUN pip install --upgrade pip && pip install -r requirements.txt
ADD . /hera/
