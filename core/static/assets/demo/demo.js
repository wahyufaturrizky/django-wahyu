type = ["primary", "info", "success", "warning", "danger"];

demo = {
  refreshChart: function() {
    drawTotalCompleteness();
    drawCompletenessCoverageByFY();
    drawContractsByCategory();
    drawTargetActual();
  },

  initPickColor: function() {
    $(".pick-class-label").click(function() {
      var new_class = $(this).attr("new-class");
      var old_class = $("#display-buttons").attr("data-class");
      var display_div = $("#display-buttons");
      if (display_div.length) {
        var display_buttons = display_div.find(".btn");
        display_buttons.removeClass(old_class);
        display_buttons.addClass(new_class);
        display_div.attr("data-class", new_class);
      }
    });
  },

  initDashboardPageCharts: function() {
    gradientChartOptionsConfigurationWithTooltipBlue = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.0)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 60,
              suggestedMax: 125,
              padding: 20,
              fontColor: "#2380f7"
            }
          }
        ],

        xAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#2380f7"
            }
          }
        ]
      }
    };

    gradientChartOptionsConfigurationWithTooltipPurple = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.0)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 60,
              suggestedMax: 125,
              padding: 20,
              fontColor: "#9a9a9a"
            }
          }
        ],

        xAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(225,78,202,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#9a9a9a"
            }
          }
        ]
      }
    };

    barChartOptionsYpercent = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom"
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "point",
        intersect: 0,
        position: "nearest",
        callbacks: {
          label: function(tooltipItem, data) {
            var dataset = data.datasets[tooltipItem.datasetIndex];
            var currentValue = dataset.data[tooltipItem.index];
            return String((Number(currentValue) * 100).toFixed(2)) + "%";
          }
        }
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            barPercentage: 1,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.0)",
              zeroLineColor: "transparent"
            },
            ticks: {
              callback: function(value, index, values) {
                return String(Math.trunc(Number(value) * 100)) + "%";
              }
            }
          }
        ],

        xAxes: [
          {
            barPercentage: 1,
            gridLines: {
              drawBorder: false,
              color: "rgba(225,78,202,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#9a9a9a"
            }
          }
        ]
      }
    };

    barChartOptions = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom"
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "point",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            barPercentage: 1,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.0)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 0,
              // suggestedMax: 125,
              padding: 20,
              fontColor: "#9a9a9a"
            }
          }
        ],

        xAxes: [
          {
            barPercentage: 1,
            gridLines: {
              drawBorder: false,
              color: "rgba(225,78,202,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#9a9a9a"
            }
          }
        ]
      }
    };

    gradientChartOptionsConfigurationWithTooltipOrange = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.0)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 50,
              suggestedMax: 110,
              padding: 20,
              fontColor: "#ff8a76"
            }
          }
        ],

        xAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(220,53,69,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#ff8a76"
            }
          }
        ]
      }
    };

    gradientChartOptionsConfigurationWithTooltipGreen = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.0)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 50,
              suggestedMax: 125,
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }
        ],

        xAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(0,242,195,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }
        ]
      }
    };

    pieChartConfiguration = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom",
        labels: {
          boxWidth: 20,
          fontColor: "#fff"
        }
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "point",
        intersect: 0,
        position: "nearest"
      },
      responsive: true
    };

    barStackedChartConfigurationRPBillion = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom",
        labels: {
          boxWidth: 20,
          fontColor: "#fff"
        }
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        intersect: 0,
        position: "nearest",
        mode: "index",
        callbacks: {
          label: function(tooltipItem, data) {
            var dataset = data.datasets[tooltipItem.datasetIndex];
            var currentValue = dataset.data[tooltipItem.index];
            return formatNumberRp(String(currentValue));
          }
        }
      },
      responsive: true,

      scales: {
        xAxes: [
          {
            stacked: true,
            ticks: {
              fontColor: "#fff"
            }
          }
        ],
        yAxes: [
          {
            stacked: true,
            labelString: "Billions",
            ticks: {
              fontColor: "#fff",
              callback: function(value, index, values) {
                return "Rp" + String((Number(value) / 1000000000).toFixed(3));
              }
            }
          }
        ]
      }
    };

    barStackedChartConfiguration = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom",
        labels: {
          boxWidth: 20,
          fontColor: "#fff"
        }
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        intersect: 0,
        position: "nearest",
        mode: "index"
      },
      responsive: true,

      scales: {
        xAxes: [
          {
            stacked: true,
            ticks: {
              fontColor: "#fff"
            }
          }
        ],
        yAxes: [
          {
            stacked: true,
            ticks: {
              fontColor: "#fff"
            }
          }
        ]
      }
    };

    horizontalBarStackedChartConfiguration = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom",
        labels: {
          boxWidth: 20,
          fontColor: "#fff"
        }
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        intersect: 0,
        position: "nearest",
        mode: "index"
      },
      responsive: true,

      scales: {
        xAxes: [
          {
            stacked: true,
            ticks: {
              fontColor: "#fff"
            }
          }
        ],
        yAxes: [
          {
            stacked: true,
            ticks: {
              fontColor: "#fff"
            }
          }
        ]
      }
    };

    gradientBarChartConfigurationYpercent = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom"
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest",
        callbacks: {
          label: function(tooltipItem, data) {
            var dataset = data.datasets[tooltipItem.datasetIndex];
            var currentValue = dataset.data[tooltipItem.index];
            return String((Number(currentValue) * 100).toFixed(2)) + "%";
          }
        }
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              callback: function(value, index, values) {
                return String(Math.trunc(Number(value) * 100)) + "%";
              }
            }
          }
        ],

        xAxes: [
          {
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }
        ]
      }
    };

    gradientBarChartConfiguration = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom"
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 0,
              // suggestedMax: 120,
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }
        ],

        xAxes: [
          {
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }
        ]
      }
    };

    $(document).ready(function () {
      $(".radiobtnfilter").click(function () {
        demo.refreshChart();
      });
    });

    this.refreshChart();
  },

  showNotification: function(from, align) {
    color = Math.floor(Math.random() * 4 + 1);

    $.notify(
      {
        icon: "tim-icons icon-bell-55",
        message: "This is how an alert would appear"
      },
      {
        type: type[color],
        timer: 8000,
        placement: {
          from: from,
          align: align
        }
      }
    );
  }
};

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === name + "=") {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

async function drawTotalCompleteness() {
  var data = await getDataTotalCompleteness();
  var ctx = getCanvas("totalCompleteness").getContext("2d");
  var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

  gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
  gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
  gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

  var myChart = new Chart(ctx, {
    type: "pie",
    responsive: true,
    legend: {
      display: false
    },
    data: {
      labels: ["uncompleted", "completed"],
      datasets: [
        {
          // label: "Completeness Count",
          fill: true,
          backgroundColor: ["#ff9f89", "#00d6b4"],
          hoverBackgroundColor: ["#ff9f89", "#00d6b4"],
          // borderColor: '#1f8ef1',
          borderColor: "rgba(0,0,0,0)",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          // data: [uncompleted,completed]
          data: [data.uncompleted, data.completed]
        }
      ]
    },
    options: pieChartConfiguration
  });
}

async function drawCompletenessCoverageByFY() {
  var data = await getDataCompletenessCoverageByFY();
  var ctx = getCanvas("completenessCoverageByFY").getContext("2d");
  var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

  gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
  gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
  gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

  var myChart = new Chart(ctx, {
    type: "horizontalBar",
    responsive: true,
    legend: {
      display: false
    },
    data: {
      labels: data.labels,

      datasets: [
        {
          fill: true,
          backgroundColor: "#00d6b4",
          hoverBackgroundColor: "#00d6b4",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.completed,
          label: "completed"
        },
        {
          fill: true,
          backgroundColor: "#ff9f89",
          hoverBackgroundColor: "#ff9f89",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.uncompleted,
          label: "Uncompleted"
        }
      ]
    },
    options: horizontalBarStackedChartConfiguration
  });
}

async function drawContractsByCategory() {
  var data = await getDataContractsByCategory();
  var ctx = getCanvas("contractsByCategory").getContext("2d");
  var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

  gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
  gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
  gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

  var myChart = new Chart(ctx, {
    type: "pie",
    responsive: true,
    legend: {
      display: false
    },
    data: {
      labels: data.comodity_name,
      datasets: [
        {
          // label: "Completeness Count",
          fill: true,
          backgroundColor: [
            "#ff9f89",
            "#00d6b4",
            "#fd77a4",
            "#419ef9",
            "#ffc15e",
            "#ffe0b5",
            "#c6d8ff"
          ],
          hoverBackgroundColor: [
            "#ff9f89",
            "#00d6b4",
            "#fd77a4",
            "#419ef9",
            "#ffc15e",
            "#ffe0b5",
            "#c6d8ff"
          ],
          borderColor: "rgba(0,0,0,0)",
          // borderColor: '#1f8ef1',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          // data: [uncompleted,completed]
          data: data.number_of_contracts
        }
      ]
    },
    options: pieChartConfiguration
  });

  var ctx = getCanvas("completenessByCategory").getContext("2d");
  var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

  gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
  gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
  gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

  var myChart = new Chart(ctx, {
    type: "horizontalBar",
    responsive: true,
    legend: {
      display: false
    },
    data: {
      labels: data.comodity_name,

      datasets: [
        {
          fill: true,
          backgroundColor: [
            "#00d6b4",
            "#00d6b4",
            "#00d6b4",
            "#00d6b4",
            "#00d6b4",
            "#00d6b4",
            "#00d6b4"
          ],
          hoverBackgroundColor: [
            "#00d6b4",
            "#00d6b4",
            "#00d6b4",
            "#00d6b4",
            "#00d6b4",
            "#00d6b4",
            "#00d6b4"
          ],
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.completed,
          label: "completed"
        },
        {
          fill: true,
          backgroundColor: [
            "#ff9f89",
            "#ff9f89",
            "#ff9f89",
            "#ff9f89",
            "#ff9f89",
            "#ff9f89",
            "#ff9f89"
          ],
          hoverBackgroundColor: [
            "#ff9f89",
            "#ff9f89",
            "#ff9f89",
            "#ff9f89",
            "#ff9f89",
            "#ff9f89",
            "#ff9f89"
          ],
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.uncompleted,
          label: "Uncompleted"
        }
      ]
    },
    options: horizontalBarStackedChartConfiguration
  });

  var ctx = getCanvas("contractValue").getContext("2d");

  var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

  gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
  gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
  gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

  var myChart = new Chart(ctx, {
    type: "bar",
    responsive: true,
    legend: {
      display: false
    },
    data: {
      labels: data.comodity_name,
      datasets: [
        {
          label: "Completed",
          fill: true,
          backgroundColor: "#00bf9a",
          hoverBackgroundColor: "#00bf9a",
          // borderColor: '#1f8ef1',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.completed_value
        },
        {
          label: "Uncompleted",
          fill: true,
          backgroundColor: "#ffc15e",
          hoverBackgroundColor: "#ffc15e",
          // borderColor: '#1f8ef1',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.uncompleted_value
        }
      ]
    },
    options: barStackedChartConfigurationRPBillion
  });

  var table = document.getElementById("topContractsTable")
  renderTable(table, data.top_contracts);
}

async function drawTargetActual() {
  var data = await getDataTargetActual();
  var ctx = getCanvas("allProducts").getContext("2d");
  var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

  var myChart = new Chart(ctx, {
    type: "bar",
    data: {
      labels: data.month_label,
      datasets: [
        {
          label: "Target",
          fill: true,
          // backgroundColor: gradientStroke,
          backgroundColor: "#ffc15e",
          borderColor: "#ffc15e",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: "#ffc15e",
          pointBorderColor: "rgba(255,255,255,0)",
          pointHoverBackgroundColor: "#ffc15e",
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: data.target_monthly
        },
        {
          label: "Actual",
          fill: true,
          // backgroundColor: gradientStroke,
          backgroundColor: "#00bf9a",
          borderColor: "#00bf9a",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: "#00bf9a",
          pointBorderColor: "rgba(255,255,255,0)",
          pointHoverBackgroundColor: "#00bf9a",
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: data.actual_monthly
        }
      ]
    },
    options: barChartOptionsYpercent
  });
  //  allProducts ENDS

  // allProductsByQuarter BEGINS
  var labelsAllProductsByQuarter = data.quarter_label;
  var dataaAllProductsByQuarter1 = data.target_qurterly;
  var dataaAllProductsByQuarter2 = data.actual_qurterly;

  var ctx = getCanvas("allProductsByQuarter").getContext("2d");

  var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

  gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
  gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
  gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

  var myChart = new Chart(ctx, {
    type: "bar",
    responsive: true,
    legend: {
      display: false
    },
    data: {
      labels: labelsAllProductsByQuarter,
      datasets: [
        {
          label: "Target",
          fill: true,
          backgroundColor: "#ffc15e",
          hoverBackgroundColor: "#ffc15e",
          // borderColor: '#1f8ef1',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: dataaAllProductsByQuarter1
        },
        {
          label: "Actual",
          fill: true,
          backgroundColor: "#00bf9a",
          hoverBackgroundColor: "#00bf9a",
          // borderColor: '#1f8ef1',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: dataaAllProductsByQuarter2
        }
      ]
    },
    options: gradientBarChartConfigurationYpercent
  });

  // allProductsByQuarter ENDS

  // yearToDate BEGINS
  var labelsYearToDate = ["YTD"];
  var dataYearToDate1 = [44];
  var dataYearToDate2 = [40];

  var ctx = getCanvas("yearToDate").getContext("2d");

  var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

  gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
  gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
  gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

  var myChart = new Chart(ctx, {
    type: "bar",
    responsive: true,
    legend: {
      display: false
    },
    data: {
      labels: labelsYearToDate,
      datasets: [
        {
          label: "Target",
          fill: true,
          backgroundColor: "#ffc15e",
          hoverBackgroundColor: "#ffc15e",
          // borderColor: '#1f8ef1',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: dataYearToDate1
        },
        {
          label: "Actual",
          fill: true,
          backgroundColor: "#00bf9a",
          hoverBackgroundColor: "#00bf9a",
          // borderColor: '#1f8ef1',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: dataYearToDate2
        }
      ]
    },
    options: gradientBarChartConfiguration
  });

  // yearToDate ENDS
}

async function getDataTotalCompleteness() {
  var filterParam = getFilterParam();
  const api_url = "/api/total_completeness";
  var response = await fetch(api_url, {
    method: "post",
    credentials: "same-origin",
    headers: {
      "X-CSRFToken": getCookie("csrftoken"),
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: filterParam
  });
  var data = await response.json();
  return data;
}

async function getDataCompletenessCoverageByFY() {
  var filterParam = getFilterParam();
  const api_url = "/api/total_completeness_by_fy";
  var response = await fetch(api_url, {
    method: "post",
    credentials: "same-origin",
    headers: {
      "X-CSRFToken": getCookie("csrftoken"),
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: filterParam
  });
  var data = await response.json();

  return data;
}

async function getDataContractsByCategory() {
  var filterParam = getFilterParam();
  const api_url = "/api/contracts_by_category";
  var response = await fetch(api_url, {
    method: "post",
    credentials: "same-origin",
    headers: {
      "X-CSRFToken": getCookie("csrftoken"),
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: filterParam
  });
  var data = await response.json();
  return data;
}

async function getDataTargetActual() {
  var filterParam = getFilterParam();
  const api_url = "/api/target_actual";
  var response = await fetch(api_url, {
    method: "post",
    credentials: "same-origin",
    headers: {
      "X-CSRFToken": getCookie("csrftoken"),
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: filterParam
  });
  var data = await response.json();
  return data;
}

function getFilterParam() {
  let form = document.getElementById("filterForm");
  let formData = new FormData(form);
  var filterParam = {};
  for (var pair of formData.entries()) {
    filterParam[pair[0]] = pair[1];
  }
  return JSON.stringify(filterParam);
}
