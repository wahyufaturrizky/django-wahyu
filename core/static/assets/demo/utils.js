async function getData(url, filterParam) {
  const api_url = `${url}?${filterParam}`;
  var response = await fetch(api_url, {
    method: "get",
    credentials: "same-origin",
    headers: {
      "X-CSRFToken": getCookie("csrftoken"),
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  });
  var data = await response.json();
  return data;
}

function getCanvas(id) {
  var ctx = document.getElementById(id);
  var parent = ctx.parentElement;
  var child_id = ctx.id;
  var child_class = ctx.classList;
  parent.removeChild(ctx);
  var newCanvas = document.createElement("canvas");
  newCanvas.classList = child_class;
  newCanvas.id = child_id;
  parent.appendChild(newCanvas);
  return newCanvas;
}

function formatNumberRp(n, zeroAsDash) {
  var zeroAsDash = zeroAsDash || false
  // format number 1000000 to 1.234.567
  res = "Rp" + new Intl.NumberFormat("id-ID", { maximumFractionDigits: 3 }).format(n);
  if ((zeroAsDash) && res == "Rp0"){
    return "-";
  }else{
    return res;
  }

}

function renderTable(tableContainer, data) {
  tableContainer.innerHTML = "";
  var rowIndex = 0;
  for (const row of data) {
    var row_elem = tableContainer.insertRow(rowIndex);
    rowIndex++;
    for (const cell of row) {
      var cell_elem = row_elem.insertCell();
      cell_elem.innerHTML = cell;
      cell_elem.style = "border:none;";
    }
  }
}

function serializeFilterParam(formId){
    return $(`#${formId}`).serialize()
}