type = ["primary", "info", "success", "warning", "danger"];

demovenus = {
  refreshChartVenus: function() {
    var filterParam = serializeFilterParam("filterForm");
    drawOpenPO(filterParam);
    drawOpenGRNI(filterParam);
    drawPOAfter(filterParam);
  },

  initPickColor: function() {
    $(".pick-class-label").click(function() {
      var new_class = $(this).attr("new-class");
      var old_class = $("#display-buttons").attr("data-class");
      var display_div = $("#display-buttons");
      if (display_div.length) {
        var display_buttons = display_div.find(".btn");
        display_buttons.removeClass(old_class);
        display_buttons.addClass(new_class);
        display_div.attr("data-class", new_class);
      }
    });
  },

  initDashboardPageCharts: function() {
    gradientChartOptionsConfigurationWithTooltipBlue = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.0)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 60,
              suggestedMax: 125,
              padding: 20,
              fontColor: "#2380f7"
            }
          }
        ],

        xAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#2380f7"
            }
          }
        ]
      }
    };

    gradientChartOptionsConfigurationWithTooltipPurple = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.0)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 60,
              suggestedMax: 125,
              padding: 20,
              fontColor: "#9a9a9a"
            }
          }
        ],

        xAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(225,78,202,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#9a9a9a"
            }
          }
        ]
      }
    };

    barChartOptions = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom"
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "point",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            barPercentage: 1,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.0)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 0,
              // suggestedMax: 125,
              padding: 20,
              fontColor: "#9a9a9a"
            }
          }
        ],

        xAxes: [
          {
            barPercentage: 1,
            gridLines: {
              drawBorder: false,
              color: "rgba(225,78,202,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#9a9a9a"
            }
          }
        ]
      }
    };

    gradientChartOptionsConfigurationWithTooltipOrange = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.0)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 50,
              suggestedMax: 110,
              padding: 20,
              fontColor: "#ff8a76"
            }
          }
        ],

        xAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(220,53,69,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#ff8a76"
            }
          }
        ]
      }
    };

    gradientChartOptionsConfigurationWithTooltipGreen = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.0)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 50,
              suggestedMax: 125,
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }
        ],

        xAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: "rgba(0,242,195,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }
        ]
      }
    };

    pieChartConfiguration = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom",
        labels: {
          boxWidth: 20,
          fontColor: "#fff"
        }
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "point",
        intersect: 0,
        position: "nearest"
      },
      responsive: true
    };

    barStackedChartConfiguration = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom",
        labels: {
          boxWidth: 20,
          fontColor: "#fff"
        }
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        intersect: 0,
        position: "nearest",
        mode: "index"
      },
      responsive: true,

      scales: {
        xAxes: [
          {
            stacked: true,
            ticks: {
              fontColor: "#fff"
            }
          }
        ],
        yAxes: [
          {
            stacked: true,
            ticks: {
              fontColor: "#fff"
            }
          }
        ]
      }
    };

    horizontalBarStackedChartConfiguration = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom",
        labels: {
          boxWidth: 20,
          fontColor: "#fff"
        }
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        intersect: 0,
        position: "nearest",
        mode: "index"
      },
      responsive: true,

      scales: {
        xAxes: [
          {
            stacked: true,
            ticks: {
              fontColor: "#fff"
            }
          }
        ],
        yAxes: [
          {
            stacked: true,
            ticks: {
              fontColor: "#fff"
            }
          }
        ]
      }
    };

    venusCombinedChartConfiguration = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom"
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest",
        callbacks: {
          label: function(tooltipItem, data) {
            var dataset = data.datasets[tooltipItem.datasetIndex];
            var currentValue = dataset.data[tooltipItem.index];
            if (dataset.label == "Cases Count") {
              return currentValue;
            } else {
              return formatNumberRp(String(currentValue));
            }
          }
        }
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            id: "a",
            scaleLabel: {
              display: true,
              labelString: "Mio NZD"
            },
            type: "linear",
            position: "left",
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 0,
              // suggestedMax: 120,
              padding: 20,
              fontColor: "#9e9e9e",
              callback: function(value, index, values) {
                return "Rp" + String((Number(value) / 1000000).toFixed(0));
              }
            }
          },
          {
            id: "b",
            scaleLabel: {
              display: true,
              labelString: "Count of POs"
            },
            type: "linear",
            position: "right",
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 0,
              // suggestedMax: 120,
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }
        ],

        xAxes: [
          {
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }
        ]
      }
    };

    gradientBarChartConfiguration = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "bottom"
      },

      tooltips: {
        backgroundColor: "#f5f5f5",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              suggestedMin: 0,
              // suggestedMax: 120,
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }
        ],

        xAxes: [
          {
            gridLines: {
              drawBorder: false,
              color: "rgba(29,140,248,0.1)",
              zeroLineColor: "transparent"
            },
            ticks: {
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }
        ]
      }
    };

    $(document).ready(function() {
      $(".radiobtnfilter2").click(function() {
        demovenus.refreshChartVenus();
      });
    });

    this.refreshChartVenus();
  },

  showNotification: function(from, align) {
    color = Math.floor(Math.random() * 4 + 1);

    $.notify(
      {
        icon: "tim-icons icon-bell-55",
        message: "This is how an alert would appear"
      },
      {
        type: type[color],
        timer: 8000,
        placement: {
          from: from,
          align: align
        }
      }
    );
  }
};

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === name + "=") {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

async function drawOpenPO(filterParam) {
  var API_URL = "/api/venus_open_po";
  var CHART_ID = "monthlyOpenPO";
  var TABLE_ID = "OpenPO";

  var data = await getData(API_URL, filterParam);
  var ctx = getCanvas(CHART_ID).getContext("2d");
  var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);
  gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
  gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
  gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

  var myChart = new Chart(ctx, {
    type: "bar",
    responsive: true,
    legend: {
      display: false
    },
    data: {
      labels: data.month_label,
      datasets: [
        {
          yAxisID: "b",
          label: "Cases Count",
          type: "line",
          fill: true,
          backgroundColor: "rgba(0,0,0,0)",
          // hoverBackgroundColor: '#419ef9',
          borderColor: "#419ef9",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.count_open_item,
          order: 1,
          stack: 1
        },
        {
          yAxisID: "a",
          label: "Closed",
          fill: true,
          backgroundColor: "#e14eca",
          hoverBackgroundColor: "#e14eca",
          // borderColor: ,
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.value_closed_item,
          order: 2,
          stack: 2
        },
        {
          yAxisID: "a",
          label: "Open",
          fill: true,
          backgroundColor: "#fd77a4",
          hoverBackgroundColor: "#fd77a4",
          // borderColor: ,
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.value_open_item,
          order: 2,
          stack: 2
        }
      ]
    },
    options: venusCombinedChartConfiguration
  });

  var table = document.getElementById(`Top${TABLE_ID}`);
  renderTable(table, data.top_item);

  var data_summary = prepDataForSummary(data);
  var table = document.getElementById(`SummaryTop${TABLE_ID}`);
  renderTable(table, data_summary[0]);
  // var table = document.getElementById(`SummaryBottom${TABLE_ID}`);
  // renderTable(table, data_summary[1]);

  // monthlyOpenPO ENDS
}

async function drawOpenGRNI(filterParam) {
  var API_URL = "/api/venus_grni";
  var CHART_ID = "monthlyGRNI";
  var TABLE_ID = "GRNI";

  var data = await getData(API_URL, filterParam);
  var ctx = getCanvas(CHART_ID).getContext("2d");
  var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);
  gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
  gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
  gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

  var myChart = new Chart(ctx, {
    type: "bar",
    responsive: true,
    legend: {
      display: false
    },
    data: {
      labels: data.month_label,
      datasets: [
        {
          yAxisID: "b",
          label: "Cases Count",
          type: "line",
          fill: true,
          backgroundColor: "rgba(0,0,0,0)",
          // hoverBackgroundColor: '#419ef9',
          borderColor: "#419ef9",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.count_open_item,
          order: 1,
          stack: 1
        },
        {
          yAxisID: "a",
          label: "Closed",
          fill: true,
          backgroundColor: "#e14eca",
          hoverBackgroundColor: "#e14eca",
          // borderColor: ,
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.value_closed_item,
          order: 2,
          stack: 2
        },
        {
          yAxisID: "a",
          label: "Open",
          fill: true,
          backgroundColor: "#fd77a4",
          hoverBackgroundColor: "#fd77a4",
          // borderColor: ,
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.value_open_item,
          order: 2,
          stack: 2
        }
      ]
    },
    options: venusCombinedChartConfiguration
  });

  var table = document.getElementById(`Top${TABLE_ID}`);
  renderTable(table, data.top_item);

  var data_summary = prepDataForSummary(data);
  var table = document.getElementById(`SummaryTop${TABLE_ID}`);
  renderTable(table, data_summary[0]);
  // var table = document.getElementById(`SummaryBottom${TABLE_ID}`);
  // renderTable(table, data_summary[1]);

  // monthlyOpenPO ENDS
}

async function drawPOAfter(filterParam) {
  var API_URL = "/api/venus_po_after";
  var CHART_ID = "monthlyPOAfter";
  var TABLE_ID = "POAfter";

  var data = await getData(API_URL, filterParam);
  var ctx = getCanvas(CHART_ID).getContext("2d");
  var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);
  gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
  gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
  gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

  var myChart = new Chart(ctx, {
    type: "bar",
    responsive: true,
    legend: {
      display: false
    },
    data: {
      labels: data.month_label,
      datasets: [
        {
          yAxisID: "b",
          label: "Cases Count",
          type: "line",
          fill: true,
          backgroundColor: "rgba(0,0,0,0)",
          // hoverBackgroundColor: '#419ef9',
          borderColor: "#419ef9",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.count_open_item,
          order: 1,
          stack: 1
        },
        {
          yAxisID: "a",
          label: "Closed",
          fill: true,
          backgroundColor: "#e14eca",
          hoverBackgroundColor: "#e14eca",
          // borderColor: ,
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.value_closed_item,
          order: 2,
          stack: 2
        },
        {
          yAxisID: "a",
          label: "Open",
          fill: true,
          backgroundColor: "#fd77a4",
          hoverBackgroundColor: "#fd77a4",
          // borderColor: ,
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: data.value_open_item,
          order: 2,
          stack: 2
        }
      ]
    },
    options: venusCombinedChartConfiguration
  });

  var table = document.getElementById(`Top${TABLE_ID}`);
  renderTable(table, data.top_item);

  var data_summary = prepDataForSummary(data);
  var table = document.getElementById(`SummaryTop${TABLE_ID}`);
  renderTable(table, data_summary[0]);
  // var table = document.getElementById(`SummaryBottom${TABLE_ID}`);
  // renderTable(table, data_summary[1]);

  // monthlyOpenPO ENDS
}

function prepDataForSummary(data) {
  var total_value_closed = data.value_closed_item.reduce((a, b) => a + b, 0);
  var total_value_open = data.value_open_item.reduce((a, b) => a + b, 0);
  var total_value_all = total_value_closed + total_value_open;

  var total_count_closed = data.count_closed_item.reduce((a, b) => a + b, 0);
  var total_count_open = data.count_open_item.reduce((a, b) => a + b, 0);
  var total_count_all = total_count_closed + total_count_open;

  var percentage_of_open_value = total_value_closed / total_value_all;
  var percentage_of_open_count = total_count_closed / total_count_all;

  dataTop = [
    [
      `${(percentage_of_open_value * 100).toFixed(2)}%`,
      "Values have been closed"
    ],
    [
      `${(percentage_of_open_count * 100).toFixed(2)}%`,
      "Cases have been closed"
    ],
    [
      `${(data.all_user_yearly_avg).toFixed(2)}`,
      "days covered"
    ]
  ];
  dataBottom = [
    [formatNumberRp(String(total_value_closed/1000000), true), "Mio NZD", "/", formatNumberRp(String(total_value_all/1000000), true), "Mio NZD"],
    [total_count_closed, "POs", "/", total_count_all, "Cases as closed"],
    [total_count_open, "POs", "/", total_count_all, "Cases as open"]
  ];
  res = [dataTop, dataBottom];

  return res;
}
